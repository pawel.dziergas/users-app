# Users app

An example of usage React with hooks, Redux, Redux Saga, TypeScript. This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Commands

To run type:

### `npm start`

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
