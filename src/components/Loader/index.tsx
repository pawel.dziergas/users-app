import React from 'react';

import './styles.scss';

const Loader = () => {
    return (
        <div className="loader" role="progressbar"
             aria-labelledby="loader__label">
            <span id="loader__label" className="loader__label">Loading...</span>
        </div>
    );
};

export default Loader;