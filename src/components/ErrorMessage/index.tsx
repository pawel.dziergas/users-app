import React, { FC } from 'react';

const ErrorMessage: FC = ({children}) => <p className="error">{children}</p>;

export default ErrorMessage;