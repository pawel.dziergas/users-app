import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Loader from '../../components/Loader';
import { IStoreState } from '../../store/types';
import { fetchUsersRequest } from '../../modules/Users/actions';
import { getUsersSelector, searchUsersSelector } from '../../modules/Users/selectors';
import UserList from './components/UserList';
import UserSearch from './components/UserSearch';
import ErrorMessage from '../../components/ErrorMessage';

import './styles.scss';

const UsersPage = () => {

    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(fetchUsersRequest());
    }, [dispatch]);
    const {requesting, error} = useSelector((state: IStoreState) => getUsersSelector(state));
    const [searchValue, setSearchValue] = useState('');
    const users = useSelector((state: IStoreState) => searchUsersSelector(state, searchValue));

    if (requesting) {
        return <Loader/>;
    }

    return (
        <div className="users">
            <h1>Users list</h1>
            <UserSearch setSearchValue={setSearchValue}/>
            {error
                ?
                <ErrorMessage>Sorry, we got error while fetch users. Try again.</ErrorMessage>
                :
                <UserList users={users}/>
            }
        </div>
    );
};

export default UsersPage;