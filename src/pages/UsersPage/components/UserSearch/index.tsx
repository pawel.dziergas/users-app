import React, { FC } from 'react';

import { IProps } from './types';

import './styles.scss';

const UserSearch: FC<IProps> = ({setSearchValue}) => (
    <div className="users__search">
        <input className="users__search-input" type="text" placeholder="Search by user name..."
               onChange={ev => setSearchValue(ev.target.value)}/>
    </div>
);

export default UserSearch;
