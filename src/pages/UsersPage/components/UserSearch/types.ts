import { Dispatch } from 'react';


export interface IProps {
    setSearchValue: Dispatch<string>;
}