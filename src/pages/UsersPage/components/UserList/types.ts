import { IUser } from '../../../../modules/Users/types';

export interface IProps {
    users: IUser[];
}