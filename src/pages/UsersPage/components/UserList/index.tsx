import React, { FC } from 'react';

import { IProps } from './types';

import './styles.scss';

const UserList: FC<IProps> = ({users}) => {
    return (
        <ul className="users__list">
            {users.map((user, index) => (
                <li className="users__list-item" key={user.id}>
                    {index + 1 + '.'}
                    <span className="user-name">{user.name}</span>
                    @ {user.username}
                </li>
            ))}
        </ul>
    );
};

export default UserList;