import axios from 'axios';

import { ApiConfig } from '../../config';

const apiClient = axios.create({
    baseURL: ApiConfig.URL
});

export default apiClient;