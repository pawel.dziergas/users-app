import { call, takeLatest, put } from 'redux-saga/effects';

import * as actions from './actions';
import { FetchUsersPayload, IUsersFetchRequest } from './types';
import { ApiConfig } from '../../config';
import apiClient from '../../utils/api/apiClient';

const fetchUsersRequest = async (): Promise<FetchUsersPayload> => {
    const {data} = await apiClient.get<FetchUsersPayload>(ApiConfig.endpoints.users);

    return data;
};

export function* usersSaga(action: IUsersFetchRequest) {

    try {
        const response = yield call(fetchUsersRequest);
        yield put(actions.fetchUsersSuccess(response));
    } catch (err) {
        yield put(actions.fetchUsersError());
    }
}

export default function* () {
    yield takeLatest(actions.ActionTypes.USERS_FETCH_REQUEST, usersSaga);
}