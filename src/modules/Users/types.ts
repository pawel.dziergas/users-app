import { ActionTypes } from './actions';

export interface IUsersFetchRequest {
    readonly type: ActionTypes.USERS_FETCH_REQUEST;
}

export type FetchUsersPayload = IUser[];

export interface IUsersFetchSuccess {
    readonly type: ActionTypes.USERS_FETCH_SUCCESS;
    payload: FetchUsersPayload;
}

export interface IUsersFetchError {
    readonly type: ActionTypes.USERS_FETCH_ERROR;
}

export interface IGeolocation {
    lat: string;
    lng: string;
}

export interface ICompany {
    name: string;
    catchPhrase: string;
    bs: string;
}

export interface IUserAddress {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
    geo: IGeolocation;
}

export interface IUser {
    id: number,
    name: string,
    username: string,
    email: string,
    address: IUserAddress,
    phone: string,
    website: string;
    company: ICompany;
}

export interface IUsersState {
    requesting: boolean;
    error: boolean;
    list: IUser[];
}