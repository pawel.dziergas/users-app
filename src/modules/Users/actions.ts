import { FetchUsersPayload, IUsersFetchError, IUsersFetchRequest, IUsersFetchSuccess } from './types';

export enum ActionTypes {
    USERS_FETCH_REQUEST = '[USERS] fetch - request',
    USERS_FETCH_SUCCESS = '[USERS] fetch - success',
    USERS_FETCH_ERROR = '[USERS] fetch - error'
}

export type Actions = IUsersFetchRequest | IUsersFetchSuccess | IUsersFetchError;

export const fetchUsersRequest = (): IUsersFetchRequest => {
    return {
        type: ActionTypes.USERS_FETCH_REQUEST
    };
};

export const fetchUsersSuccess = (payload: FetchUsersPayload): IUsersFetchSuccess => {
    return {
        type: ActionTypes.USERS_FETCH_SUCCESS,
        payload
    };
};

export const fetchUsersError = (): IUsersFetchError => {
    return {
        type: ActionTypes.USERS_FETCH_ERROR
    };
};