import { Actions, ActionTypes } from './actions';
import { IUsersState } from './types';

const initialState: IUsersState = {
    requesting: true,
    error: false,
    list: []
};

const usersReducer = (state: IUsersState = initialState, action: Actions): IUsersState => {
    switch (action.type) {
        case ActionTypes.USERS_FETCH_REQUEST: {
            return {
                ...state,
                error: false,
                requesting: true
            };
        }

        case ActionTypes.USERS_FETCH_SUCCESS: {
            return {
                error: false,
                requesting: false,
                list: action.payload
            };
        }

        case ActionTypes.USERS_FETCH_ERROR: {
            return {
                error: true,
                requesting: false,
                list: []
            };
        }

        default: {
            return state;
        }
    }
};

export default usersReducer;