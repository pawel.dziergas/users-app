import { IStoreState } from '../../store/types';

export const getUsersSelector = (state: IStoreState) => state.users;

export const searchUsersSelector = (state: IStoreState, searchPhrase: string) => getUsersSelector(state).list.filter(
    user => user.name.toLowerCase().indexOf(searchPhrase.toLowerCase()) > -1
);