import { IUsersState } from '../modules/Users/types';

export interface IStoreState {
    users: IUsersState;
}