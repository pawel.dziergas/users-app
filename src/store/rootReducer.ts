import { combineReducers } from 'redux';

import { IStoreState } from './types';
import usersReducer from '../modules/Users/reducers';

const rootReducer = combineReducers<IStoreState>({
    users: usersReducer
});

export default rootReducer;