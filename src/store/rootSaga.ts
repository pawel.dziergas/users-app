import { all } from 'redux-saga/effects';

import UsersSaga from '../modules/Users/saga';

export default function* rootSaga() {
    yield all([
        UsersSaga()
    ]);
}