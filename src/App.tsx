import React from 'react';
import UsersPage from './pages/UsersPage';

import './styles.scss';

const App: React.FC = () => {
    return (
        <div className="app">
            <UsersPage/>
        </div>
    );
};

export default App;
